package binar.synrgy.ch1.challenge.service;

import java.io.IOException;

public interface VolumService {
    boolean kubus () throws IOException;

    boolean balok () throws IOException;

    boolean tabung () throws IOException;
}
