package binar.synrgy.ch1.challenge.service.impl;

import binar.synrgy.ch1.challenge.service.LuasService;

import java.io.IOException;
import java.util.Scanner;

public class LuasServiceImpl implements LuasService {
    Scanner input = new Scanner(System.in);

    @Override
    public boolean persegi() throws IOException {
        int sisi;
        System.out.println("-------------------------------\n" +
                "Anda memilih persegi\n" +
                "-------------------------------");
        System.out.print("Masukkan sisi : ");

        sisi = input.nextInt();

        System.out.println("processing...");

        int luas = sisi * sisi;

        System.out.println("Luas dari persegi adalah " + luas + "\n" +
                "-------------------------------\n" +
                "tekan enter untuk kembali ke menu utama");
        System.in.read();
        return true;
    }

    @Override
    public boolean lingkaran() throws IOException {
        int radius;
        System.out.println("-------------------------------\n" +
                "Anda memilih lingkaran\n" +
                "-------------------------------");
        System.out.print("Masukkan radius : ");

        radius = input.nextInt();

        System.out.println("processing...");

        double luas = Math.PI * radius * radius;

        System.out.println("Luas dari lingkaran adalah " + luas + "\n" +
                "-------------------------------\n" +
                "tekan enter untuk kembali ke menu utama");
        System.in.read();
        return true;
    }

    @Override
    public boolean segitiga() throws IOException {
        int alas;
        int tinggi;

        System.out.println("-------------------------------\n" +
                "Anda memilih segitiga\n" +
                "-------------------------------");
        System.out.print("Masukkan alas : ");
        alas = input.nextInt();
        System.out.print("Masukkan tinggi : ");
        tinggi = input.nextInt();

        System.out.println("processing...");

        double luas = 0.5 * alas * tinggi;

        System.out.println("Luas dari segitiga adalah " + luas + "\n" +
                "-------------------------------\n" +
                "tekan enter untuk kembali ke menu utama");
        System.in.read();
        return true;
    }

    @Override
    public boolean persegi_panjang() throws IOException {
        int panjang;
        int lebar;
        System.out.println("-------------------------------\n" +
                "Anda memilih persegi panjang\n" +
                "-------------------------------");
        System.out.print("Masukkan panjang : ");
        panjang = input.nextInt();
        System.out.print("Masukkan lebar : ");
        lebar = input.nextInt();

        System.out.println("processing...");

        int luas = panjang * lebar;

        System.out.println("Luas dari persegi panjang adalah " + luas + "\n" +
                "-------------------------------\n" +
                "tekan enter untuk kembali ke menu utama");
        System.in.read();
        return true;
    }
}
