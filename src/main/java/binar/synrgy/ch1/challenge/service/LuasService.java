package binar.synrgy.ch1.challenge.service;

import java.io.IOException;

public interface LuasService {
    boolean persegi () throws IOException;
    boolean lingkaran () throws IOException;
    boolean segitiga () throws IOException;
    boolean persegi_panjang () throws IOException;
}
