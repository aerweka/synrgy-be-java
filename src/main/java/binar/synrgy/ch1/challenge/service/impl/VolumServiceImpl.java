package binar.synrgy.ch1.challenge.service.impl;

import binar.synrgy.ch1.challenge.service.VolumService;

import java.io.IOException;
import java.util.Scanner;

public class VolumServiceImpl implements VolumService {
    Scanner input = new Scanner(System.in);

    @Override
    public boolean kubus() throws IOException {
        int sisi;
        System.out.println("-------------------------------\n" +
                "Anda memilih kubus\n" +
                "-------------------------------");
        System.out.print("Masukkan sisi : ");

        sisi = input.nextInt();

        System.out.println("processing...");

        int volum = sisi * sisi * sisi;

        System.out.println("Volum dari kubus adalah " + volum + "\n" +
                "-------------------------------\n" +
                "tekan enter untuk kembali ke menu utama");
        System.in.read();
        return true;
    }

    @Override
    public boolean balok() throws IOException {
        int panjang;
        int lebar;
        int tinggi;
        System.out.println("-------------------------------\n" +
                "Anda memilih balok\n" +
                "-------------------------------");
        System.out.print("Masukkan panjang : ");
        panjang = input.nextInt();
        System.out.print("Masukkan lebar : ");
        lebar = input.nextInt();
        System.out.print("Masukkan tinggi : ");
        tinggi = input.nextInt();

        System.out.println("processing...");

        int volum = panjang * lebar * tinggi;

        System.out.println("Volum dari balok adalah " + volum+ "\n" +
                "-------------------------------\n" +
                "tekan enter untuk kembali ke menu utama");
        System.in.read();
        return true;
    }

    @Override
    public boolean tabung() throws IOException {
        int radius;
        int tinggi;
        System.out.println("-------------------------------\n" +
                "Anda memilih tabung\n" +
                "-------------------------------");
        System.out.print("Masukkan radius : ");
        radius = input.nextInt();
        System.out.print("Masukkan tinggi : ");
        tinggi = input.nextInt();

        System.out.println("processing...");

        double volum = Math.PI * radius * radius * tinggi;

        System.out.println("Volum dari tabung adalah " + volum+ "\n" +
                "-------------------------------\n" +
                "tekan enter untuk kembali ke menu utama");
        System.in.read();
        return true;
    }
}
