package binar.synrgy.ch1.challenge;

import binar.synrgy.ch1.challenge.service.LuasService;
import binar.synrgy.ch1.challenge.service.VolumService;
import binar.synrgy.ch1.challenge.service.impl.LuasServiceImpl;
import binar.synrgy.ch1.challenge.service.impl.VolumServiceImpl;

import java.io.IOException;
import java.util.Scanner;

public class App {
    static Scanner input = new Scanner(System.in);
    static boolean isContinue = true;

    public static void main(String[] args) throws IOException {
        while (isContinue) app();
    }

    public static boolean app() throws IOException {
        VolumService volumService = new VolumServiceImpl();
        LuasService luasService = new LuasServiceImpl();
        int bidang, pilihan;

        System.out.println("-------------------------------\n" +
                "Kalkulator Penghitung Luas dan Volume\n" +
                "-------------------------------\n" +
                "Menu\n" +
                "1. Menghitung Luas Bidang\n" +
                "2. Hitung Volum\n" +
                "0. Tutup Aplikasi");

        System.out.print("Masukkan pilihanmu : ");
        pilihan = input.nextInt();

        switch (pilihan) {
            case 1:
                System.out.println("-------------------------------\n" +
                        "Pilih bidang yang akan dihitung\n" +
                        "-------------------------------\n" +
                        "1. persegi\n" +
                        "2. lingkaran\n" +
                        "3. segitiga\n" +
                        "4. persegi panjang\n" +
                        "0. kembali ke menu sebelumnya");
                break;
            case 2:
                System.out.println("-------------------------------\n" +
                        "Pilih bidang yang akan dihitung\n" +
                        "-------------------------------\n" +
                        "1. kubus\n" +
                        "2. balok\n" +
                        "3. tabung\n" +
                        "0. kembali ke menu sebelumnya");
                break;
            default:
                System.exit(0);
        }

        System.out.print("Masukkan pilihanmu : ");

        bidang = input.nextInt();

        if (bidang == 0) return isContinue;
        if (pilihan == 1) {
            if (bidang == 1) isContinue = luasService.persegi();
            if (bidang == 2) isContinue = luasService.lingkaran();
            if (bidang == 3) isContinue = luasService.segitiga();
            if (bidang == 4) isContinue = luasService.persegi_panjang();
        } else {
            if (bidang == 1) isContinue = volumService.kubus();
            if (bidang == 2) isContinue = volumService.balok();
            if (bidang == 3) isContinue = volumService.tabung();
        }

        return isContinue;
    }
}
