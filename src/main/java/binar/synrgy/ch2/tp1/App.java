package binar.synrgy.ch2.tp1;

import java.util.Scanner;

public class App {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("------------------------ Palindrom ----------------------------\n" +
                "Masukkan kata : ");
        String text = scan.next();
        boolean isPalindrome = isPalindrome(text);
        System.out.println("\nApakah palindrom? " + isPalindrome);

        System.out.print("------------------------ FizzBuzz Game ------------------------\n" +
                "Masukkan durasi pengulangan : ");
        int n = scan.nextInt();
        System.out.println("\nHasilnya : ");
        fizzBuzz(n);

    }

//    public static int[] arraySorting(int[] array) {
//
//    }

    public static void mergeSort(int[] a, int n) {
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];

        for (int i = 0; i < mid; i++) {
            l[i] = a[i];
        }
        for (int i = mid; i < n; i++) {
            r[i - mid] = a[i];
        }
        mergeSort(l, mid);
        mergeSort(r, n - mid);

        merge(a, l, r, mid, n - mid);
    }

    public static void merge(int[] a, int[] l, int[] r, int left, int right) {

        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                a[k++] = l[i++];
            } else {
                a[k++] = r[j++];
            }
        }
        while (i < left) {
            a[k++] = l[i++];
        }
        while (j < right) {
            a[k++] = r[j++];
        }
    }

    public static void fizzBuzz(int n) {
        for (int i = 1; i <= n; i++) {
            if (i % 3 == 0) {
                System.out.print("Fizz ");
                continue;
            }
            if (i % 5 == 0) {
                System.out.print("Buzz ");
                continue;
            }
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.print("FizzBuzz ");
                continue;
            }
            System.out.print(i + " ");
        }
    }

    public static boolean isPalindrome(String text) {
        String reversedText = "";

        for (int i = (text.length() - 1); i >= 0; --i) {
            reversedText += text.charAt(i);
        }

        if (text.toLowerCase().equals(reversedText.toLowerCase())) {
            return true;
        }
        return false;
    }
}
